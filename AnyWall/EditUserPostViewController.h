//
//  EditUserPostViewController.h
//  AnyWall
//
//  Created by Antonio Garcia & Jacob Koko
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface EditUserPostViewController : UIViewController

@property PFObject *userPost;
@property NSString* originalPostText;
@property (weak, nonatomic) IBOutlet UITextView *userInputTextView;

@end
