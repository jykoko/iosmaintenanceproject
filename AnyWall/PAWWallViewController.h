//
//  PAWWallViewController.h
//  Anywall
//
//  Created by Christopher Bowns on 1/30/12.
//  Copyright (c) 2013 Parse. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <Parse/Parse.h>
#import "PAWPost.h"

@interface PAWWallViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UINavigationBar *myNavBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *postBtn;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *settingsBtn;
@property MKMapItem *currentAnnotation;
@property NSMutableArray* allUsersOnMap;

@end

@protocol PAWWallViewControllerHighlight <NSObject>

- (void)highlightCellForPost:(PAWPost *)post;
- (void)unhighlightCellForPost:(PAWPost *)post;

@end
