//
//  OpenedPostViewController.h
//  AnyWall
//
//  Created by Jacob Koko 
#import <UIKit/UIKit.h>

@interface OpenedPostViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property  PFObject* postOnDisplay;
@property NSArray* allImgs;
@property (strong, nonatomic) IBOutlet UITextView *postDisplayTextView;
@property (strong, nonatomic) IBOutlet UICollectionView *postImagesCollectionView;

@end
