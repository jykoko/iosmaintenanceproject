#import "GeoPointAnnotation.h"

@interface GeoPointAnnotation()
@property (nonatomic, strong) PFObject *object;
@end

@implementation GeoPointAnnotation

#pragma mark - Initialization

- (id)initWithObject:(PFObject *)aObject
{
	self = [super init];
	if (self) {
		_object = aObject;
		
		PFGeoPoint *geoPoint = self.object[@"UsersCurrentLocation"];
		[self setGeoPoint:geoPoint];
	}
	return self;
}


#pragma mark - MKAnnotation


#pragma mark - ()

- (void)setGeoPoint:(PFGeoPoint *)geoPoint
{
	_coordinate = CLLocationCoordinate2DMake(geoPoint.latitude, geoPoint.longitude);
	
	static NSDateFormatter *dateFormatter = nil;
	if (dateFormatter == nil) {
		dateFormatter = [[NSDateFormatter alloc] init];
		dateFormatter.timeStyle = NSDateFormatterMediumStyle;
		dateFormatter.dateStyle = NSDateFormatterMediumStyle;
	}
	
	static NSNumberFormatter *numberFormatter = nil;
	if (numberFormatter == nil) {
		numberFormatter = [[NSNumberFormatter alloc] init];
		numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
		numberFormatter.maximumFractionDigits = 3;
	}
	
	// _title = spotTitle;
	_title = self.object[@"username"];
	//_subtitle = self.object[@"Spot_type"];
}

@end