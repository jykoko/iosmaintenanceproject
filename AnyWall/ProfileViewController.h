//
//  ProfileViewController.h
//  AnyWall
//
//  Created by Christian Decker & Jacob Koko
//
#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
	NSArray *instructions;
}

@property (weak, nonatomic) IBOutlet UITableView *myTableView;
- (IBAction)takePictureAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UINavigationBar *myNavBar;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

@end


