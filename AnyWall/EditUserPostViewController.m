//
//  EditUserPostViewController.m
//  AnyWall
//
//  Created by Antonio Garcia & Jacob Koko
#import "EditUserPostViewController.h"
#import "PAWWallPostCreateViewController.h"
#import "MBProgressHUD.h"

@interface EditUserPostViewController ()

@end

@implementation EditUserPostViewController
@synthesize userPost;
@synthesize userInputTextView;
@synthesize originalPostText;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
											 initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveUpdatedPost:)];
	/*
	   If user post object set, then get its data from parse
	   and fill text view with current post's text.
	 */
	if(userPost)
	{
		NSLog(@"Test edit user post %@", userPost[@"text"]);
		userInputTextView.text = userPost[@"text"];
		originalPostText = userPost[@"text"];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
   This function is called when user wants to save an updated post 
 */
- (IBAction)saveUpdatedPost:(id)sender
{
	MBProgressHUD* dataLoadingProgress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	dataLoadingProgress.labelText = @"Saving Post...";
	dataLoadingProgress.mode = MBProgressHUDModeIndeterminate;
	[dataLoadingProgress show:YES];
	
	NSString *updatedUserPost = userInputTextView.text;
	
	if([self isValidInputString:updatedUserPost])
	{
		userPost[@"text"]= updatedUserPost;
		[userPost saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error){
			if(!error)
			{
				NSString* successMessage = @"Success, your post has been saved!";
			   [self createAlertViewWithMessage:successMessage andTitle:@"SAVE SUCCESSFUL"];
			   [dataLoadingProgress hide:YES];
			}
			else
			{
				[dataLoadingProgress hide:YES];
			}
		}];
	}
	else
	{
		[dataLoadingProgress hide:YES];
		if([originalPostText isEqualToString:updatedUserPost])
		{
			NSString* errorMessage = @"Error saving post! You must edit your post before saving...";
			[self createAlertViewWithMessage:errorMessage andTitle:@"ERROR"];
		}
		else
		{
			NSString* errorMessage = @"Error saving post! Please check your input...";
			[self createAlertViewWithMessage:errorMessage andTitle:@"ERROR"];
		}
	}
}

/*
   This function is used to sanitize data inputs before pushing to parse.
   Essentially, we do some simple checks to make sure user can't input
   bad values.
 */
- (BOOL)isValidInputString:(NSString*)inputString
{
	BOOL isValidInput = FALSE;
	
	if([originalPostText isEqualToString:inputString])
	{
		return isValidInput;
	}
	
	if(inputString.length > 1 || ![inputString isEqualToString:@""])
	{
		NSString *probablyEmpty = [inputString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		isValidInput = ![probablyEmpty isEqualToString:@""];
	}
	
	return isValidInput;
}


/*
   Basic utility function to avoid duplicate code
   Use this function to create UIAlertViews,rather
   than creating them all over your code.
 */
-(void)createAlertViewWithMessage:(NSString*)message andTitle:(NSString*)title
{
	UIAlertView* alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
	[alertView show];
}


@end