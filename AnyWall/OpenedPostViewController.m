//
//  OpenedPostViewController.m
//  AnyWall
//
//  Created by Jacob Koko
#import "OpenedPostViewController.h"
#import <Social/Social.h>
#import "CustomImageCell.h"

@implementation OpenedPostViewController
@synthesize postDisplayTextView,postOnDisplay,postImagesCollectionView, allImgs;

-(void)viewDidLoad
{
	[super viewDidLoad];
	
	//sev navigation bar elements
	UIBarButtonItem *cameraItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(addPhotoToPost:)];
	
	UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareToFaceBook:)];
	
	NSArray *actionButtonItems = @[shareItem, cameraItem];
	self.navigationItem.rightBarButtonItems = actionButtonItems;
	
	//set text view text with posts text from parse
	postDisplayTextView.text = postOnDisplay[@"text"];
	
	postImagesCollectionView.delegate = self;
	postImagesCollectionView.dataSource = self;
	[self.postImagesCollectionView registerNib:[UINib nibWithNibName:@"CustomImageCellView" bundle:nil] forCellWithReuseIdentifier:@"ImageCell"];
	
	[self queryForPostImages];
}


/*
 * Query for images that are associated with the current post on display
 */
-(void)queryForPostImages
{
	PFQuery* imgQuery = [PFQuery queryWithClassName:@"PostPhoto"];
	[imgQuery includeKey:@"AssociatedPost"];
	[imgQuery whereKey:@"AssociatedPost" equalTo:postOnDisplay];
	[imgQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error)
		{
			if([objects count] > 0)
			{
				allImgs = [[NSArray alloc]initWithArray:objects];
				[postImagesCollectionView reloadData];
			}
			else
			{
				allImgs = [[NSArray alloc] init];
			}
		}
	}];
}

/*
 * Action that opens facebook share options for user
 * I populate the facebook share view with the posts text
 */
- (IBAction)shareToFaceBook:(id)sender
{
	if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
		
		NSString* postText = [NSString stringWithFormat:@"%@", postDisplayTextView.text];
		
		SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
		
		[controller setInitialText:postText];
		
		[self presentViewController:controller animated:YES completion:Nil];
	}
}


#pragma - UICollectionView delegate methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
	if([allImgs count] == 0)
	{
		return 1;
	}
	else
	{
		return [allImgs count];
	}
}


- (CustomImageCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
	
	CustomImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageCell" forIndexPath:indexPath];
	
	PFFile *userImageFile = [allImgs objectAtIndex:indexPath.row][@"PostImage"];
	[userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
		if (!error) {
			UIImage *image = [UIImage imageWithData:imageData];
			cell.parseImage.image = image;
		}
	}];
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
	//do nothing
}


#pragma mark - ImagePicker Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
	UIImage* scaledAndOrientedImage = [self scaleImage:chosenImage toSize:CGSizeMake(200, 200)];
	NSData *imageData = UIImageJPEGRepresentation(scaledAndOrientedImage,0.8);
	
	NSString* fileName = [NSString stringWithFormat:@"PostImage"];
	PFFile *imageFile = [PFFile fileWithName:fileName data:imageData];
	[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if(!error)
		{
			[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
				if(!error)
				{
					PFObject *postPhoto = [PFObject objectWithClassName:@"PostPhoto"];
					postPhoto[@"PostImage"] = imageFile;
					postPhoto[@"AssociatedPost"] = postOnDisplay;
					[postPhoto saveInBackground];
				}
			}];
		}
	}];

	
	[picker dismissViewControllerAnimated:YES completion:NULL];
	
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
	[picker dismissViewControllerAnimated:YES completion:NULL];
	
}

/*
 * To Do: Put these methods in seperate photo utility class!
 *
 * This function is used to scale down the size of images before uploading them to
 * the database
 */
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize
{
	CGSize scaledSize = newSize;
	float scaleFactor = 1.0;
	if( image.size.width > image.size.height )
	{
		scaleFactor = image.size.width / image.size.height;
		scaledSize.width = newSize.width;
		scaledSize.height = newSize.height / scaleFactor;
	}
	else
	{
		scaleFactor = image.size.height / image.size.width;
		scaledSize.height = newSize.height;
		scaledSize.width = newSize.width / scaleFactor;
	}
	
	UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
	CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
	[image drawInRect:scaledImageRect];
	UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return scaledImage;
}


/*
 * Open photo view so user can add photo to the post
 */
- (IBAction)addPhotoToPost:(id)sender
{
	if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
		
		UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
															  message:@"Device has no camera"
															 delegate:nil
													cancelButtonTitle:@"OK"
													otherButtonTitles: nil];
		[myAlertView show];
	}
	else
	{
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		
		[self presentViewController:picker animated:YES completion:NULL];
	}
}


@end
