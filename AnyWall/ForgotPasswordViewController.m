//
//  ForgotPasswordViewController.m
//  AnyWall
//
//  Created by Christian Decker
#import "ForgotPasswordViewController.h"

@implementation ForgotPasswordViewController
@synthesize emailInput;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
}

- (IBAction)resetButton:(id)sender
{
	NSString *unformated = emailInput.text;
    [PFUser requestPasswordResetForEmailInBackground:unformated];
}
@end
