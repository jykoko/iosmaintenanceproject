//
//  ForgotPasswordViewController.h
//  AnyWall
//
//  Created by Christian Decker
#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *emailInput;
- (IBAction)resetButton:(id)sender;

@end
