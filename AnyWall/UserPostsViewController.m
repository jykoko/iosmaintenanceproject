//
//  UserPostsViewController.m
//  AnyWall
//
//  Created by Connor Mashburn & Jacob Koko
#import "UserPostsViewController.h"
#import <Parse/Parse.h>
#import "EditUserPostViewController.h"
#import "MBProgressHUD.h"

@interface UserPostsViewController ()

@end

@implementation UserPostsViewController
@synthesize UserTableView;
MBProgressHUD* dataLoadingProgress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	userPosts = [[NSMutableArray alloc]init];
	
	//set tableview datasource and delegate
    self.UserTableView.delegate = self;
    self.UserTableView.dataSource = self;
	
	/*
	 Add loader so user doesn't see blank screen while
	 waiting for parse query to finish...
	*/
	dataLoadingProgress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	dataLoadingProgress.labelText = @"Loading User Posts";
	dataLoadingProgress.mode = MBProgressHUDModeIndeterminate;
	[dataLoadingProgress show:YES];
	
	//query for table data
	[self QueryUserPost];
}

/*
   Called before view appears to refresh tableview data
 */
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:NO];
	[self.UserTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
   Basic parse query that extracts all of current user's posts from parse
   so we can display/manipulate the post objects.
 */
-(void)QueryUserPost
{
	PFQuery *userPostQuery = [PFQuery queryWithClassName:@"Posts"];
	[userPostQuery includeKey:@"user"];
	[userPostQuery whereKey:@"user" equalTo:[PFUser currentUser]];
	[userPostQuery orderByDescending:@"updatedAt"];
	[userPostQuery findObjectsInBackgroundWithBlock:^(NSArray *incomingUserPost, NSError *error){
		 if(!error)
		 {
			 if([incomingUserPost count] > 0)
			 {
				 //update gui on main thread after process completed
				 dispatch_async(dispatch_get_main_queue(), ^{
					 [userPosts addObjectsFromArray:incomingUserPost];
					 [self.UserTableView reloadData];
					 [dataLoadingProgress hide:YES];
				 });
			 }
			 else
			 {
				 [dataLoadingProgress hide:YES];
			 }
		 }
	 }];
}


#pragma mark - TableView Delegate Methods
/*
   The next few methods are required as part of a contract between the class and its
   UITableView delegate. For the tableview to work, the following functions MUST be
   implemented.
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [userPosts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *tableIdentifier = @"UserPostViewCell";
	
	UITableViewCell *cell = [UserTableView dequeueReusableCellWithIdentifier:tableIdentifier];
	
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableIdentifier];
    }
	
    cell.textLabel.text = [userPosts objectAtIndex:indexPath.row][@"text"];
	
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	EditUserPostViewController *editUserPostViewController = [[EditUserPostViewController alloc] initWithNibName:nil bundle:nil];
	editUserPostViewController.title = @"Edit Post";
	
	PFObject* selectedPost = [userPosts objectAtIndex: indexPath.row];
	[editUserPostViewController setUserPost:selectedPost];
	
	[self.navigationController pushViewController:editUserPostViewController animated:YES];
	NSLog(@"row number is %i" , indexPath.row);
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 65;
}

@end