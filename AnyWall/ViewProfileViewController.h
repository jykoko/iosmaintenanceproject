//
//  UserProfileViewController.h
//  AnyWall
//
//  Created by Jacob Koko
#import <UIKit/UIKit.h>

@interface ViewProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
	NSMutableArray *userPosts;
}
@property PFUser* userOnDisplay;
@property (strong, nonatomic) IBOutlet UIImageView *profileimageview;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UITableView *userPostsTableView;
@end
