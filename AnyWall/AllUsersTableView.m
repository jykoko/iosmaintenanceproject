//
//  AllUsersTableView.m
//  AnyWall
//
//  Created by Jacob Koko
//
#import "AllUsersTableView.h"
#import <Parse/Parse.h>
#import "MBProgressHUD.h"
#import "ViewProfileViewController.h"

@interface AllUsersTableView ()
@end

@implementation AllUsersTableView
@synthesize globalUsersTableView;
MBProgressHUD* dataLoadingProgress;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}


- (void)viewDidLoad
{
	[super viewDidLoad];
	
	allUsers = [[NSMutableArray alloc]init];
	
	//set tableview datasource and delegate
	self.globalUsersTableView.delegate = self;
	self.globalUsersTableView.dataSource = self;
	
	//[self.globalUsersTableView setBackgroundColor:[UIColor whiteColor]];
	
	/*
	 Add loader so user doesn't see blank screen while
	 waiting for parse query to finish...
	 */
	dataLoadingProgress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	dataLoadingProgress.labelText = @"Loading Users...";
	dataLoadingProgress.mode = MBProgressHUDModeIndeterminate;
	[dataLoadingProgress show:YES];
	
	//query for table data
	[self queryForAllUsers];
}

/*
 Called before view appears to refresh tableview data
 */
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:NO];
	[self.globalUsersTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


/*
 Basic parse query that extracts all of current user's posts from parse
 so we can display/manipulate the post objects.
 */
-(void)queryForAllUsers
{
	PFQuery *usersQuery = [PFUser query];
	[usersQuery findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error){
		if(!error)
		{
			if([users count] > 0)
			{
				//update gui on main thread after process completed
				dispatch_async(dispatch_get_main_queue(), ^{
					[allUsers addObjectsFromArray:users];
					[self.globalUsersTableView reloadData];
					[dataLoadingProgress hide:YES];
				});
			}
		}
	}];
}


#pragma mark - TableView Delegate Methods
/*
 The next few methods are required as part of a contract between the class and its
 UITableView delegate. For the tableview to work, the following functions MUST be
 implemented.
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [allUsers count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	static NSString *cellIdentifier = @"usercell";
	
	
	CustomUserTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
	
	if (cell == nil)
	{
		[tableView registerNib:[UINib nibWithNibName:@"UserCellView" bundle:nil] forCellReuseIdentifier:@"usercell"];
		cell = [tableView dequeueReusableCellWithIdentifier:@"usercell"];
		
	}
	
	cell.userImagePreview.image = [UIImage imageNamed:@"default-placeholder.png"];
	cell.loadSpinner.hidden = NO;
	[cell.loadSpinner startAnimating];
	
	cell.userImagePreview.layer.cornerRadius = cell.userImagePreview.frame.size.height/2;
	cell.userImagePreview.layer.masksToBounds = YES;
	cell.userImagePreview.layer.borderWidth = 0;
	
    //grab profile image for table view cell
	PFFile *userImageFile = [allUsers objectAtIndex:indexPath.row][@"profileimage"];
	[userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
		if (!error)
		{
			cell.userImagePreview.image = [UIImage imageWithData:imageData];
			[cell.loadSpinner stopAnimating];
			cell.loadSpinner.hidden = YES;
		}
		else {
			cell.loadSpinner.hidden = NO;
		}
	}];
	
	NSString* userNameTitle = [allUsers objectAtIndex:indexPath.row][@"username"];
	cell.userNameLabel.text = userNameTitle;
	[cell.loadSpinner stopAnimating];
	cell.loadSpinner.hidden = YES;
	 

	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	PFUser *userObj = [allUsers objectAtIndex:indexPath.row];
	ViewProfileViewController *profileVc = [[ViewProfileViewController alloc] initWithNibName:@"ViewUserProfileView" bundle:nil];
	[profileVc setUserOnDisplay:userObj];
	[self.navigationController pushViewController:profileVc animated:YES];

	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 75;
}

@end