//
//  ProfileViewController.m
//  AnyWall
//
//  Created by Christian Decker & Jacob Koko
#import "ProfileViewController.h"
#import "PAWWallViewController.h"
#import "PAWSettingsViewController.h"
#import "UserPostsViewController.h"
#import "PAWAppDelegate.h"
#import "MBProgressHUD.h"
#import "AllUsersTableView.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize usernameLabel;
@synthesize myNavBar;
@synthesize profileImageView;
@synthesize myTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Initialize table data
	instructions = [NSArray arrayWithObjects:@"My Posts",@"Global Feed",@"Find Users",nil];
	
	//round profile image view corners
	profileImageView.layer.cornerRadius = profileImageView.frame.size.height /2;
	profileImageView.layer.masksToBounds = YES;
	profileImageView.layer.borderWidth = 0;
	[profileImageView setImage:[UIImage imageNamed:@"default-placeholder.png"]];
	
	//connect table delegate and datasource
	self.myTableView.delegate = self;
	self.myTableView.dataSource = self;
	
	//add navigation bar button items
	UIBarButtonItem *cameraItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(takePictureAction:)];
	self.navigationItem.rightBarButtonItem = cameraItem;
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
											 initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logUserOut:)];
	
	//set up background image blur view (IOS 8 OR LATER ONLY)
	UIImageView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_img.jpg"]];
	[self.view addSubview:backgroundView];
	
	UIVisualEffect *blurEffect;
	blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
	
	UIVisualEffectView *visualEffectView;
	visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
	
	visualEffectView.frame = backgroundView.bounds;
	[backgroundView addSubview:visualEffectView];
	[visualEffectView addSubview:self.profileImageView];
	[visualEffectView addSubview:self.usernameLabel];
	
	[self.view addSubview:visualEffectView];
	[self.view addSubview:self.myTableView];
	
	
	//if user logged in, get username from User object to display
	if([PFUser currentUser])
	{
		NSString* usrName = [PFUser currentUser][@"username"];
		self.navigationItem.title = [NSString stringWithFormat:@"@%@",usrName];
		self.usernameLabel.text = usrName;
		[self setUserProfileImage];
	}
}

- (void) viewWillAppear:(BOOL)animated
{
	[self.navigationController setNavigationBarHidden:NO animated:animated];
}

-(void)setUserProfileImage
{
	PFFile *userImageFile = [PFUser currentUser][@"profileimage"];
	if(userImageFile && ![userImageFile isEqual:[NSNull null]])
	{
		[userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
			if (!error) {
				UIImage *image = [UIImage imageWithData:imageData];
				self.profileImageView.image = image;
			}
		}];
	}
	else
	{
		//handle user image not being set if needed, for now keep default as placeholder
		self.profileImageView.image = [UIImage imageNamed:@"default-placeholder.png"];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - User Actions

- (IBAction)logUserOut:(id)sender
{
	[PFUser logOut];
	
	PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	[appDelegate presentWelcomeViewController];
	
}

#pragma mark - TableView Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [instructions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	
	static NSString *tableIdentifier = @"ProfileViewCell";
	UITableViewCell *cell = [myTableView dequeueReusableCellWithIdentifier:tableIdentifier];
	
    if (cell == nil)
	{
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableIdentifier];
    }
	
	switch (indexPath.row)
	{
		case 0:
			cell.imageView.image = [UIImage imageNamed:@"speech_bub_icon.png"];
			break;
		case 1:
			cell.imageView.image = [UIImage imageNamed:@"gobal_feed_icon.png"];
			break;
		case 2:
			cell.imageView.image = [UIImage imageNamed:@"userTableViewIcon.png"];
			break;
		default:
			break;
	}
	
	cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [instructions objectAtIndex:indexPath.row];
	
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 65;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		UserPostsViewController *userViewController = [[UserPostsViewController alloc] initWithNibName:nil bundle:nil];
		userViewController.title = @"My Posts";
		[self.navigationController pushViewController:userViewController animated:YES];
	}
	else if(indexPath.row == 1)
	{
		PAWWallViewController *wallViewController = [[PAWWallViewController alloc] initWithNibName:nil bundle:nil];
		wallViewController.title = @"Global Feed";
		[self.navigationController pushViewController:wallViewController animated:YES];
	}
	else
	{
		AllUsersTableView *globalPostsView = [[AllUsersTableView alloc] initWithNibName:@"AllUsersView" bundle:nil];
		globalPostsView.title = @"Find Users";
		[self.navigationController pushViewController:globalPostsView animated:YES];
	}
	
	
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - ImagePicker Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	MBProgressHUD* progLoader = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
	progLoader.labelText = @"UPLOADING IMAGE";
	progLoader.detailsLabelText = @"Updating user profile image...";
	progLoader.mode = MBProgressHUDModeIndeterminate;
	[progLoader show:YES];
	
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
	UIImage* scaledAndOrientedImage = [self scaleImage:chosenImage toSize:CGSizeMake(200, 200)];
	NSData *imageData = UIImageJPEGRepresentation(scaledAndOrientedImage,0.8);
	self.profileImageView.image = [UIImage imageWithData:imageData];
	
	//save profile image to parse database
	PFUser *currentUser = [PFUser currentUser];
	if (currentUser) {
		NSString* currentUserName = [PFUser currentUser][@"username"];
		NSString* fileName = [NSString stringWithFormat:@"%@ProfileImage", currentUserName];
		PFFile *imageFile = [PFFile fileWithName:fileName data:imageData];
		[imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
			if(!error)
			{
				[currentUser setObject:imageFile forKey:@"profileimage"];
				[currentUser saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error)
				{
					if(!error)
					{
						[progLoader hide:YES];
					}
					else
					{
						[progLoader hide:YES];
					}
				}];
			}
		}];
	 }
	
    [picker dismissViewControllerAnimated:YES completion:NULL];
	
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
	
    [picker dismissViewControllerAnimated:YES completion:NULL];
	
}

/*
 * To Do: Put these methods in seperate photo utility class!
 *
 * This function is used to scale down the size of images before uploading them to
 * the database
 */
- (UIImage*)scaleImage:(UIImage*)image toSize:(CGSize)newSize
{
	CGSize scaledSize = newSize;
	float scaleFactor = 1.0;
	if( image.size.width > image.size.height )
	{
		scaleFactor = image.size.width / image.size.height;
		scaledSize.width = newSize.width;
		scaledSize.height = newSize.height / scaleFactor;
	}
	else
	{
		scaleFactor = image.size.height / image.size.width;
		scaledSize.height = newSize.height;
		scaledSize.width = newSize.width / scaleFactor;
	}
	
	UIGraphicsBeginImageContextWithOptions( scaledSize, NO, 0.0 );
	CGRect scaledImageRect = CGRectMake( 0.0, 0.0, scaledSize.width, scaledSize.height );
	[image drawInRect:scaledImageRect];
	UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return scaledImage;
}


- (IBAction)takePictureAction:(id)sender
{
	if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
		
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
															  message:@"Device has no camera"
															 delegate:nil
													cancelButtonTitle:@"OK"
													otherButtonTitles: nil];
        [myAlertView show];
    }
	else
	{
		UIImagePickerController *picker = [[UIImagePickerController alloc] init];
		picker.delegate = self;
		picker.allowsEditing = YES;
		picker.sourceType = UIImagePickerControllerSourceTypeCamera;
		
		[self presentViewController:picker animated:YES completion:NULL];
	}
}

@end
