//
//  AllUsersTableView.h
//  AnyWall
//
//  Created by Jacob Koko
//
#import <UIKit/UIKit.h>
#import "CustomUserTableCell.h"

@interface AllUsersTableView : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
	NSMutableArray *allUsers;
}

@property (weak, nonatomic) IBOutlet UITableView *globalUsersTableView;

@end
