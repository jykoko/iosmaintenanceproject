//
//  CustomUserTableCell.h
//
// Custom Table View cell for displaying user images
// Must subclass when using XIB files
//
//
//  Created by Jacob Koko
#import <UIKit/UIKit.h>

@interface CustomUserTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *userImagePreview;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView* loadSpinner;

@end
