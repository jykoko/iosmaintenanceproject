//
//  PAWWallViewController.m
//  Anywall
//
//  Created by Christopher Bowns on 1/30/12.
//  Copyright (c) 2013 Parse. All rights reserved.
//
#import "PAWWallViewController.h"
#import "PAWSettingsViewController.h"
#import "PAWWallPostCreateViewController.h"
#import "PAWAppDelegate.h"
#import "PAWWallPostsTableViewController.h"
#import "PAWSearchRadius.h"
#import "PAWCircleView.h"
#import "PAWPost.h"
#import <CoreLocation/CoreLocation.h>
#import "GeoPointAnnotation.h"
#import "ViewProfileViewController.h"
#import "OpenedPostViewController.h"

@interface PAWWallViewController ()
@property (nonatomic, strong) CLLocationManager *_locationManager;
@property (nonatomic, strong) PAWSearchRadius *searchRadius;
@property (nonatomic, strong) PAWCircleView *circleView;
@property (nonatomic, strong) NSMutableArray *annotations;
@property (nonatomic, copy) NSString *className;
@property (nonatomic, strong) PAWWallPostsTableViewController *wallPostsTableViewController;
@property (nonatomic, assign) BOOL mapPinsPlaced;
@property (nonatomic, assign) BOOL mapPannedSinceLocationUpdate;
@property (nonatomic, strong) NSMutableArray *allPosts;

- (void)startStandardUpdates;
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation;
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error;
- (IBAction)settingsButtonSelected:(id)sender;
- (IBAction)postButtonSelected:(id)sender;
- (void)queryForAllPostsNearLocation:(CLLocation *)currentLocation withNearbyDistance:(CLLocationAccuracy)nearbyDistance;
- (void)updatePostsForLocation:(CLLocation *)location withNearbyDistance:(CLLocationAccuracy) filterDistance;

// NSNotification callbacks
- (void)distanceFilterDidChange:(NSNotification *)note;
- (void)locationDidChange:(NSNotification *)note;
- (void)postWasCreated:(NSNotification *)note;
@end

@implementation PAWWallViewController
@synthesize mapView;
@synthesize _locationManager = locationManager;
@synthesize searchRadius;
@synthesize circleView;
@synthesize annotations;
@synthesize className;
@synthesize wallPostsTableViewController;
@synthesize allPosts;
@synthesize mapPinsPlaced;
@synthesize mapPannedSinceLocationUpdate;
@synthesize myNavBar;
@synthesize postBtn,settingsBtn,currentAnnotation,allUsersOnMap;
NSMutableArray* userLocationsArray;

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		self.title = @"Anywall";
		self.className = kPAWParsePostsClassKey;
		annotations = [[NSMutableArray alloc] initWithCapacity:10];
		allPosts = [[NSMutableArray alloc] initWithCapacity:10];
	}
	return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	userLocationsArray = [[NSMutableArray alloc]init];

	// Add the wall posts tableview as a subview with view containment (new in iOS 5.0):
	self.wallPostsTableViewController = [[PAWWallPostsTableViewController alloc] initWithStyle:UITableViewStylePlain];
	[self addChildViewController:self.wallPostsTableViewController];
	self.wallPostsTableViewController.view.frame = CGRectMake(0.0f, 315.0f, 320.0f, self.view.bounds.size.height - 215.0f);
	[self.view addSubview:self.wallPostsTableViewController.view];

	
	/*
	  @author Jacob Koko
	  set up nav bar items for display
	 */
	UIBarButtonItem *shareItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(postButtonSelected:)];
	UIImage *userIconImg = [UIImage imageNamed:@"usericon_new.png"];
	UIButton *userProfileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
	[userProfileBtn addTarget:self action:@selector(sendUserBackToProfile:) forControlEvents:UIControlEventTouchUpInside];
	[userProfileBtn setBackgroundImage:userIconImg forState:UIControlStateNormal];
	userProfileBtn.frame = CGRectMake(0 ,0,26,26);
	
	UIBarButtonItem *userProfileBtnItem=[[UIBarButtonItem alloc]initWithCustomView:userProfileBtn];
	NSArray *actionButtonItems = @[shareItem, userProfileBtnItem];
	self.navigationItem.rightBarButtonItems = actionButtonItems;
	self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
											 initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonSelected:)];
	
	//hook up actions for post button and settings button
	self.postBtn=[[UIBarButtonItem alloc]
				 initWithTitle:@"Post" style:UIBarButtonItemStylePlain target:self action:@selector(postButtonSelected:)];
	self.settingsBtn=[[UIBarButtonItem alloc]
					 initWithTitle:@"Settings" style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonSelected:)];
	
	/*
	   Set up notification center alerts in order to keep data synchronized across the entire app. Every time user moves, notification center is alerted and data is then updated.
	 */
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(distanceFilterDidChange:) name:kPAWFilterDistanceChangeNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationDidChange:) name:kPAWLocationChangeNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(postWasCreated:) name:kPAWPostCreatedNotification object:nil];

	
	//set mapview display for specific region
	self.mapView.region = MKCoordinateRegionMake(CLLocationCoordinate2DMake(37.332495f, -122.029095f), MKCoordinateSpanMake(0.008516f, 0.021801f));
	self.mapPannedSinceLocationUpdate = NO;
	
	allUsersOnMap = [[NSMutableArray alloc]init];
	
	//start location updates for user
	[self startStandardUpdates];
}



- (void)viewWillAppear:(BOOL)animated
{
	[locationManager startUpdatingLocation];
	[self.navigationController setNavigationBarHidden:NO animated:YES];
	[super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[locationManager stopUpdatingLocation];
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
	[locationManager stopUpdatingLocation];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kPAWFilterDistanceChangeNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kPAWLocationChangeNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:kPAWPostCreatedNotification object:nil];
	
	self.mapPinsPlaced = NO; // reset this for the next time we show the map.
}

#pragma mark - NSNotificationCenter notification handlers

- (void)distanceFilterDidChange:(NSNotification *)note
{
	CLLocationAccuracy filterDistance = [[[note userInfo] objectForKey:kPAWFilterDistanceKey] doubleValue];
	PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

	if (self.searchRadius == nil)
	{
		self.searchRadius = [[PAWSearchRadius alloc] initWithCoordinate:appDelegate.currentLocation.coordinate radius:appDelegate.filterDistance];
		[mapView addOverlay:self.searchRadius];
	}
	else
	{
		self.searchRadius.radius = appDelegate.filterDistance;
	}

	// Update our pins for the new filter distance:
	[self updatePostsForLocation:appDelegate.currentLocation withNearbyDistance:filterDistance];
	
	// If they panned the map since our last location update, don't recenter it.
	if (!self.mapPannedSinceLocationUpdate)
	{
		// Set the map's region centered on their location at 2x filterDistance
		MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation.coordinate, appDelegate.filterDistance * 2.0f, appDelegate.filterDistance * 2.0f);

		[mapView setRegion:newRegion animated:YES];
		self.mapPannedSinceLocationUpdate = NO;
	}
	else
	{
		// Just zoom to the new search radius (or maybe don't even do that?)
		MKCoordinateRegion currentRegion = mapView.region;
		MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(currentRegion.center, appDelegate.filterDistance * 2.0f, appDelegate.filterDistance * 2.0f);

		BOOL oldMapPannedValue = self.mapPannedSinceLocationUpdate;
		[mapView setRegion:newRegion animated:YES];
		self.mapPannedSinceLocationUpdate = oldMapPannedValue;
	}
}

- (void)locationDidChange:(NSNotification *)note
{
	PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

	// If they panned the map since our last location update, don't recenter it.
	if (!self.mapPannedSinceLocationUpdate)
	{
		// Set the map's region centered on their new location at 2x filterDistance
		MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation.coordinate, appDelegate.filterDistance * 2.0f, appDelegate.filterDistance * 2.0f);

		BOOL oldMapPannedValue = self.mapPannedSinceLocationUpdate;
		[mapView setRegion:newRegion animated:YES];
		self.mapPannedSinceLocationUpdate = oldMapPannedValue;
	}

	// If we haven't drawn the search radius on the map, initialize it.
	if (self.searchRadius == nil)
	{
		self.searchRadius = [[PAWSearchRadius alloc] initWithCoordinate:appDelegate.currentLocation.coordinate radius:appDelegate.filterDistance];
		[mapView addOverlay:self.searchRadius];
	}
	else
	{
		self.searchRadius.coordinate = appDelegate.currentLocation.coordinate;
	}

	// Update the map with new pins:
	[self queryForAllPostsNearLocation:appDelegate.currentLocation withNearbyDistance:appDelegate.filterDistance];
	
	// And update the existing pins to reflect any changes in filter distance:
	[self updatePostsForLocation:appDelegate.currentLocation withNearbyDistance:appDelegate.filterDistance];
	
	//remove all user annotations and readd them with updated lat/lon
	[self.mapView removeAnnotations:userLocationsArray];
	[self queryForUserLocs];
}

- (void)postWasCreated:(NSNotification *)note
{
	PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	[self queryForAllPostsNearLocation:appDelegate.currentLocation withNearbyDistance:appDelegate.filterDistance];
}

#pragma mark - UINavigationBar-based actions

- (IBAction)settingsButtonSelected:(id)sender
{
	PAWSettingsViewController *settingsViewController = [[PAWSettingsViewController alloc] initWithNibName:nil bundle:nil];
	settingsViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	[self.navigationController presentViewController:settingsViewController animated:YES completion:nil];
}

- (IBAction)postButtonSelected:(id)sender
{
	PAWWallPostCreateViewController *createPostViewController = [[PAWWallPostCreateViewController alloc] initWithNibName:nil bundle:nil];
	[self.navigationController presentViewController:createPostViewController animated:YES completion:nil];
}

- (IBAction)sendUserBackToProfile:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - CLLocationManagerDelegate methods and helpers

- (void)startStandardUpdates
{
	if (nil == locationManager)
	{
		locationManager = [[CLLocationManager alloc] init];
	}

	locationManager.delegate = self;
	locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	
	// Set a movement threshold for new events.
	locationManager.distanceFilter = kCLLocationAccuracyNearestTenMeters;
	
	if(IS_OS_8_OR_LATER)
	{
	   [locationManager requestAlwaysAuthorization];
	}
	
	[locationManager startUpdatingLocation];

	CLLocation *currentLocation = locationManager.location;
	if (currentLocation)
	{
		PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
		appDelegate.currentLocation = currentLocation;
	}
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
	NSLog(@"%s", __PRETTY_FUNCTION__);
	switch (status)
	{
		case kCLAuthorizationStatusAuthorized:
			NSLog(@"kCLAuthorizationStatusAuthorized");
			// Re-enable the post button if it was disabled before.
			self.navigationItem.rightBarButtonItem.enabled = YES;
			[locationManager startUpdatingLocation];
			break;
		case kCLAuthorizationStatusDenied:
			NSLog(@"kCLAuthorizationStatusDenied");
			{{
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Anywall can’t access your current location.\n\nTo view nearby posts or create a post at your current location, turn on access for Anywall to your location in the Settings app under Location Services." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
				[alertView show];
				// Disable the post button.
				self.navigationItem.rightBarButtonItem.enabled = NO;
			}}
			break;
		case kCLAuthorizationStatusNotDetermined:
			NSLog(@"kCLAuthorizationStatusNotDetermined");
			break;
		case kCLAuthorizationStatusRestricted:
			NSLog(@"kCLAuthorizationStatusRestricted");
			break;
	}
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
	NSLog(@"%s", __PRETTY_FUNCTION__);

	PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
	appDelegate.currentLocation = newLocation;
	
	PFGeoPoint* userLoc = [PFGeoPoint geoPointWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
	
	[PFUser currentUser][@"UsersCurrentLocation"]=userLoc;
	PFUser* currentUser = [PFUser currentUser];
	[currentUser saveInBackground];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error{
	NSLog(@"%s", __PRETTY_FUNCTION__);
	NSLog(@"Error: %@", [error description]);

	if (error.code == kCLErrorDenied)
	{
		[locationManager stopUpdatingLocation];
	}
	else if (error.code == kCLErrorLocationUnknown)
	{
		// todo: retry?
		// set a timer for five seconds to cycle location, and if it fails again, bail and tell the user.
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error retrieving location"
		                                                message:[error description]
		                                               delegate:nil
		                                      cancelButtonTitle:nil
		                                      otherButtonTitles:@"Ok", nil];
		[alert show];
	}
}

#pragma mark - MKMapViewDelegate methods

- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
	MKOverlayView *result = nil;
	float version = [[[UIDevice currentDevice] systemVersion] floatValue];
	
	// Only display the search radius in iOS 5.1+
	if (version >= 5.1f && [overlay isKindOfClass:[PAWSearchRadius class]])
	{
		result = [[PAWCircleView alloc] initWithSearchRadius:(PAWSearchRadius *)overlay];
		[(MKOverlayPathView *)result setFillColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.2f]];
		[(MKOverlayPathView *)result setStrokeColor:[[UIColor darkGrayColor] colorWithAlphaComponent:0.7f]];
		[(MKOverlayPathView *)result setLineWidth:2.0];
	}
	return result;
}

- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation
{
	static NSString *pinIdentifier = @"CustomPinAnnotation";
	static NSString *userPinIdentifier = @"UserPinAnnotation";
	
	// Let the system handle user location annotations.
	if ([annotation isKindOfClass:[MKUserLocation class]])
	{
		return nil;
	}
	
	if([annotation isKindOfClass:[PAWPost class]])
	{
		// Try to dequeue an existing pin view first.
		MKPinAnnotationView *pinView = (MKPinAnnotationView*)[aMapView dequeueReusableAnnotationViewWithIdentifier:pinIdentifier];

		if (!pinView)
		{
			// If an existing pin view was not available, create one.
			pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
			                                          reuseIdentifier:pinIdentifier];
			pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		}
		else
		{
			pinView.annotation = annotation;
		}
		
		pinView.pinColor = [(PAWPost *)annotation pinColor];
		pinView.animatesDrop = YES;
		pinView.canShowCallout = YES;

		return pinView;
		
	}
	else
	{
		// Try to dequeue an existing pin view first.
		MKPinAnnotationView *annotationView = (MKPinAnnotationView*)[aMapView dequeueReusableAnnotationViewWithIdentifier:userPinIdentifier];
		
		GeoPointAnnotation *anno = (GeoPointAnnotation*)annotation;
		if (!annotationView)
		{
			annotationView = [[MKPinAnnotationView alloc]
							  initWithAnnotation:anno
							  reuseIdentifier:userPinIdentifier];
			annotationView.canShowCallout = YES;
			annotationView.pinColor = MKPinAnnotationColorPurple;
			annotationView.draggable = NO;
			annotationView.enabled = YES;
			annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		}
		else
		{
			annotationView.annotation = annotation;
		}
		
		/*
		 * @author Jacob Koko
		 *
		 * set default image for pin until query finishes loading...
		 */
		UIImage* defaultImage = [UIImage imageNamed:@"default-placeholder.png"];
		UIImage* defaultPinImg = [PAWWallViewController imageWithImage:defaultImage scaledToSize:CGSizeMake(40, 40)];
		UIImageView* defaultImgView = [[UIImageView alloc]initWithImage:defaultPinImg];
		defaultImgView.layer.cornerRadius = defaultImgView.frame.size.height /2;
		defaultImgView.layer.masksToBounds = YES;
		defaultImgView.layer.borderWidth = 0;
		defaultImgView.frame = CGRectMake(-9,-2,40,40);
		[annotationView addSubview:defaultImgView];
	
		/*
		 * @author Jacob Koko
		 *
		 * set profile image on top of user location in real time
		 */
		PFQuery *query = [PFUser query];
		[query whereKey:@"username" equalTo:anno.title];
		[query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
			if(!error)
			{
				if([users count] > 0)
				{
					PFFile *profImg = [users objectAtIndex:0][@"profileimage"];
					[profImg getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
						if(!error)
						{
							[defaultImgView removeFromSuperview];
							UIImage* profileImage = [UIImage imageWithData:imageData];
							[self setCustomImage:profileImage forAnnotationView:annotationView];
						}
						else
						{
							[defaultImgView removeFromSuperview];
							[self setDefaultImageForAnnotationView:annotationView];
						}
					}];
				}
			 }
			 else
			 {
				[defaultImgView removeFromSuperview];
				[self setDefaultImageForAnnotationView:annotationView];
			 }
		}];
		
	    return annotationView;
	}

	return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
	id<MKAnnotation> annotation = [view annotation];
	if ([annotation isKindOfClass:[PAWPost class]])
	{
		PAWPost *post = [view annotation];
		[wallPostsTableViewController highlightCellForPost:post];
	}
	else if ([annotation isKindOfClass:[MKUserLocation class]])
	{
		// Center the map on the user's current location:
		PAWAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
		MKCoordinateRegion newRegion = MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation.coordinate, appDelegate.filterDistance * 2, appDelegate.filterDistance * 2);

		[self.mapView setRegion:newRegion animated:YES];
		self.mapPannedSinceLocationUpdate = NO;
	}
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
	id<MKAnnotation> annotation = [view annotation];
	if ([annotation isKindOfClass:[PAWPost class]])
	{
		PAWPost *post = [view annotation];
		[wallPostsTableViewController unhighlightCellForPost:post];
	}
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
{
	self.mapPannedSinceLocationUpdate = YES;
}


/*
 * @author Jacob Koko
 *
 * This function is called when any callout accessory has been tapped(ie. map pin)
 * This is a delegate method, its required for map annotation call outs.
 */
-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view
calloutAccessoryControlTapped:(UIControl *)control
{
	GeoPointAnnotation *ann = (GeoPointAnnotation *)view.annotation;
	self.currentAnnotation = view.annotation;
	[self.mapView deselectAnnotation:ann animated:NO];
	
	if([view.annotation isKindOfClass:[PAWPost class]])
	{
		[self sender:view annotationHasTitle:ann.title];
	}
	else
	{
		for(NSString* title in self.allUsersOnMap) {
			if([title isEqualToString:ann.title]) {
				[self sender:view annotationTitle:title];
				NSLog(@"opening title: %@",title);
				break;
			}
		}
	}
}


/*
 * @author Jacob Koko
 *
 * This function is called when a post pin button has been tapped.
 * The purpose of this function is to get current post object being tapped on, and pass
 * it to the posts view in order to populate its data.
 */
-(void)sender:(MKAnnotationView *)sender annotationHasTitle:(NSString*)annotationText
{
	dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
	dispatch_async(myQueue, ^{
		// Perform long running process
		PFQuery *userQuery = [PFQuery queryWithClassName:@"Posts"];
		[userQuery whereKey:@"text" equalTo:annotationText];
		[userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			if(!error) {
				if (objects.count == 0) {
					
					userQuery.cachePolicy = kPFCachePolicyCacheThenNetwork;
					
				} else {
					
					PFObject *postObj = [objects objectAtIndex:0];
					OpenedPostViewController *postVc = [[OpenedPostViewController alloc] initWithNibName:@"OpenedPostView" bundle:nil];
					[postVc setPostOnDisplay:postObj];
					[self.navigationController pushViewController:postVc animated:YES];
				}
			}
		}];
	});
}
 

/*
 * @author Jacob Koko
 *
 * This function is called when a user profile pin button has been tapped.
 * The purpose of this function is to get current user being tapped on, and pass
 * it to the profileview in order to populate user profile data.
 */
-(void)sender:(MKAnnotationView *)sender annotationTitle:(NSString*)annotationTitle
{
	dispatch_queue_t myQueue = dispatch_queue_create("My Queue",NULL);
	dispatch_async(myQueue, ^{
		// Perform long running process
		PFQuery *userQuery = [PFUser query];
		[userQuery whereKey:@"username" equalTo:annotationTitle];
		
		[userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			if(!error) {
				if (objects.count == 0) {
					
					userQuery.cachePolicy = kPFCachePolicyCacheThenNetwork;
					
				} else {
					
					PFUser *userObj = [objects objectAtIndex:0];
					ViewProfileViewController *profileVc = [[ViewProfileViewController alloc] initWithNibName:@"ViewUserProfileView" bundle:nil];
					[profileVc setUserOnDisplay:userObj];
					[self.navigationController pushViewController:profileVc animated:YES];
				}
			}
		}];
	});
}



#pragma mark - Fetch map pins

- (void)queryForAllPostsNearLocation:(CLLocation *)currentLocation withNearbyDistance:(CLLocationAccuracy)nearbyDistance {
	
	PFQuery *query = [PFQuery queryWithClassName:self.className];

	if (currentLocation == nil) {
		NSLog(@"%s got a nil location!", __PRETTY_FUNCTION__);
	}

	/*
		If no objects are loaded in memory, we look to the cache first to fill the table
		and then subsequently do a query against the network.
	 */
	if ([self.allPosts count] == 0)
	{
		query.cachePolicy = kPFCachePolicyCacheThenNetwork;
	}

	// Query for posts sort of kind of near our current location.
	PFGeoPoint *point = [PFGeoPoint geoPointWithLatitude:currentLocation.coordinate.latitude longitude:currentLocation.coordinate.longitude];
	[query whereKey:kPAWParseLocationKey nearGeoPoint:point withinKilometers:kPAWWallPostMaximumSearchDistance];
	[query includeKey:kPAWParseUserKey];
	
	/*
	 * Edit made by Jacob Koko
	 *
	 * "remove" posts that are older than 24 hours
	 */
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *offsetComponents = [[NSDateComponents alloc] init];
	[offsetComponents setDay:-1]; // replace "-1" with "1" to get the datePlusOneDay
	NSDate* currentDate = [NSDate date];
	NSDate *dateMinusOneDay = [gregorian dateByAddingComponents:offsetComponents toDate:currentDate options:0];
    [query whereKey:@"updatedAt" greaterThan:dateMinusOneDay];
	
	query.limit = kPAWWallPostsSearch;
	[query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (error)
		{
			NSLog(@"error in geo query!"); // todo why is this ever happening?
		}
		else
		{
			/*
			  We need to make new post objects from objects,
			  and update allPosts and the map to reflect this new array.
			  But we don't want to remove all annotations from the mapview blindly,
			  so let's do some work to figure out what's new and what needs removing.
			 */
			NSMutableArray *newPosts = [[NSMutableArray alloc] initWithCapacity:kPAWWallPostsSearch];
			NSMutableArray *allNewPosts = [[NSMutableArray alloc] initWithCapacity:kPAWWallPostsSearch];
			for (PFObject *object in objects)
			{
				PAWPost *newPost = [[PAWPost alloc] initWithPFObject:object];
				[allNewPosts addObject:newPost];
				BOOL found = NO;
				for(PAWPost *currentPost in allPosts)
				{
					if([newPost equalToPost:currentPost])
					{
						found = YES;
					}
				}
				
				if (!found)
				{
					[newPosts addObject:newPost];
				}
			}
			
			
			// 2. Find posts in allPosts that didn't make the cut.
			NSMutableArray *postsToRemove = [[NSMutableArray alloc] initWithCapacity:kPAWWallPostsSearch];
			
			for(PAWPost *currentPost in allPosts)
			{
				BOOL found = NO;
				
				// Use our object cache from the first loop to save some work.
				for(PAWPost *allNewPost in allNewPosts)
				{
					if ([currentPost equalToPost:allNewPost])
					{
						found = YES;
					}
				}
				
				if(!found)
				{
					[postsToRemove addObject:currentPost];
				}
			}

			// 3. Configure our new posts; these are about to go onto the map.
			for (PAWPost *newPost in newPosts)
			{
				CLLocation *objectLocation = [[CLLocation alloc] initWithLatitude:newPost.coordinate.latitude longitude:newPost.coordinate.longitude];
				CLLocationDistance distanceFromCurrent = [currentLocation distanceFromLocation:objectLocation];
				[newPost setTitleAndSubtitleOutsideDistance:(distanceFromCurrent > nearbyDistance ? YES : NO )];
				newPost.animatesDrop = mapPinsPlaced;
			 }

			/*
			   At this point, newAllPosts contains a new list of post objects.
			   We should add everything in newPosts to the map, remove everything in postsToRemove,
			   and add newPosts to allPosts
			 */
			[mapView removeAnnotations:postsToRemove];
			[mapView addAnnotations:newPosts];
			[allPosts addObjectsFromArray:newPosts];
			[allPosts removeObjectsInArray:postsToRemove];
			self.mapPinsPlaced = YES;
		}
	}];
}


/*
  @author Jacob Koko
 
  I wrote this function to populate the map with all the user's within 25k miles. 
  What I do is query parse database, and pull all user's within specific radius.
  I then create a map pin annotation, and associate user with the annotation.
 */
- (void)queryForUserLocs
{
	PFGeoPoint* userLoc = [PFGeoPoint geoPointWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude];
	
	PFQuery *query = [PFUser query];
	[query whereKey:@"UsersCurrentLocation" nearGeoPoint:userLoc withinMiles:25000];
	[query findObjectsInBackgroundWithBlock:^(NSArray *users, NSError *error) {
		if(!error)
		{
			if([users count] > 0)
			{
				for(PFObject *user in users)
				{
					[allUsersOnMap addObject:user[@"username"]];
				    GeoPointAnnotation *geoPointAnnotation = [[GeoPointAnnotation alloc]
						initWithObject:user];
					[userLocationsArray addObject:geoPointAnnotation];
					[self.mapView addAnnotation:geoPointAnnotation];
				}
			}
		 }
	}];
}


// When we update the search filter distance, we need to update our pins' titles to match.
- (void)updatePostsForLocation:(CLLocation *)currentLocation withNearbyDistance:(CLLocationAccuracy) nearbyDistance {
	for (PAWPost *post in allPosts) {
		CLLocation *objectLocation = [[CLLocation alloc] initWithLatitude:post.coordinate.latitude longitude:post.coordinate.longitude];
		// if this post is outside the filter distance, don't show the regular callout.
		CLLocationDistance distanceFromCurrent = [currentLocation distanceFromLocation:objectLocation];
		if (distanceFromCurrent > nearbyDistance) { // Outside search radius
			[post setTitleAndSubtitleOutsideDistance:YES];
			[mapView viewForAnnotation:post];
			[(MKPinAnnotationView *) [mapView viewForAnnotation:post] setPinColor:post.pinColor];
		} else {
			[post setTitleAndSubtitleOutsideDistance:NO]; // Inside search radius
			[mapView viewForAnnotation:post];
			[(MKPinAnnotationView *) [mapView viewForAnnotation:post] setPinColor:post.pinColor];
		}
	}
}


#pragma - Image Utility Functions
/*
 @author Jacob Koko
 function that sets profile image for map pin
 */
-(void)setCustomImage:(UIImage*)profileImage forAnnotationView:(MKPinAnnotationView*)annotationView
{
	UIImage* pinImg = [PAWWallViewController imageWithImage:profileImage scaledToSize:CGSizeMake(40, 40)];
	UIImageView* pinImgView = [[UIImageView alloc]initWithImage:pinImg];
	pinImgView.layer.cornerRadius = pinImgView.frame.size.height /2;
	pinImgView.layer.masksToBounds = YES;
	pinImgView.layer.borderWidth = 0;
	pinImgView.frame = CGRectMake(-9,-2,40,40);
	[annotationView addSubview:pinImgView];
}


/*
 @author Jacob Koko
 function that sets default image for map pin
 */
-(void)setDefaultImageForAnnotationView:(MKPinAnnotationView*)annotationView
{
	//if no user logged in, then display default
	UIImage* defaultImage = [UIImage imageNamed:@"default-placeholder.png"];
	UIImage* pinImg = [PAWWallViewController imageWithImage:defaultImage scaledToSize:CGSizeMake(40, 40)];
	UIImageView* pinImgView = [[UIImageView alloc]initWithImage:pinImg];
	pinImgView.layer.cornerRadius = pinImgView.frame.size.height /2;
	pinImgView.layer.masksToBounds = YES;
	pinImgView.layer.borderWidth = 0;
	pinImgView.frame = CGRectMake(-9,-2,40,40);
	[annotationView addSubview:pinImgView];
}


/*
   Utility method for scaling UIImage's to a new desired size.
 */
+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
	UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
	[image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return newImage;
}

@end