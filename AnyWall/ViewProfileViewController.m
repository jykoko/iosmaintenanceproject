//
//  UserProfileViewController.m
//  AnyWall
//
//  Created by Jacob Koko
#import "ViewProfileViewController.h"

@implementation ViewProfileViewController
@synthesize userPostsTableView, userOnDisplay, profileimageview, userNameLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}


- (void)viewDidLoad
{
	[super viewDidLoad];
	
	if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
		self.edgesForExtendedLayout = UIRectEdgeNone;
	
	self.view.backgroundColor = [UIColor whiteColor];
	
	userPosts = [[NSMutableArray alloc]init];
	
	//set tableview datasource and delegate
	self.userPostsTableView.delegate = self;
	self.userPostsTableView.dataSource = self;
	
	
	//set user profile image
	profileimageview.image = [UIImage imageNamed:@"default-placeholder.png"];
	PFFile *userImageFile = userOnDisplay[@"profileimage"];
	[userImageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
		if (!error)
		{
			profileimageview.image = [UIImage imageWithData:imageData];
		}
		else
		{
			profileimageview.image = [UIImage imageNamed:@"default-placeholder.png"];
		}
	}];
	
	//set use name label
    userNameLabel.text = userOnDisplay[@"username"];
	
	//query for table data
	[self queryForUserPosts];
}

/*
 Called before view appears to refresh tableview data
 */
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:NO];
	//[self.userPostsTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}


/*
   Basic parse query that extracts all of current user's posts from parse
   so we can display/manipulate the post objects.
 */
-(void)queryForUserPosts
{
	PFQuery *userPostQuery = [PFQuery queryWithClassName:@"Posts"];
	[userPostQuery includeKey:@"user"];
	[userPostQuery whereKey:@"user" equalTo:userOnDisplay];
	[userPostQuery orderByDescending:@"updatedAt"];
	[userPostQuery findObjectsInBackgroundWithBlock:^(NSArray *incomingUserPost, NSError *error){
		if(!error)
		{
			if([incomingUserPost count] > 0)
			{
				NSLog(@"posts count: %i", [incomingUserPost count]);
				//update gui on main thread after process completed
				dispatch_async(dispatch_get_main_queue(), ^{
					[userPosts addObjectsFromArray:incomingUserPost];
					[self.userPostsTableView reloadData];
				});
			}
		}
	}];
}


#pragma mark - TableMethods
/*
 The next few methods are required as part of a contract between the class and its
 UITableView delegate. For the tableview to work, the following functions MUST be
 implemented.
 */
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [userPosts count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
	
	static NSString *tableIdentifier = @"UserPostViewCell";
	
	UITableViewCell *cell = [userPostsTableView dequeueReusableCellWithIdentifier:tableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:tableIdentifier];
	}
	
	cell.textLabel.text = [userPosts objectAtIndex:indexPath.row][@"text"];
	
	return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSLog(@"row number is %i" , indexPath.row);
	[tableView deselectRowAtIndexPath:indexPath animated:NO];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 65;
}

@end
