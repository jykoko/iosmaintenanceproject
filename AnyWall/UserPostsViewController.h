//
//  UserPostsViewController.h
//  AnyWall
//
//  Created by Connor Mashburn & Jacob Koko
#import <UIKit/UIKit.h>

@interface UserPostsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *userPosts;
}
@property (weak, nonatomic) IBOutlet UITableView *UserTableView;

@end
