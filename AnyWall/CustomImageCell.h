//
//  CustomUserTableCell.h
//
// Custom Collection View cell for displaying post images
// Must subclass when using XIB files
//
//
//  Created by Jacob Koko
#import <UIKit/UIKit.h>

@interface CustomImageCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *parseImage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;

@end
